package main

import (
	"gitee.com/jhj420/cart/domain/repository"
	service2 "gitee.com/jhj420/cart/domain/service"
	"gitee.com/jhj420/cart/handler"
	cart "gitee.com/jhj420/cart/proto/cart"
	"gitee.com/jhj420/common"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/micro/go-micro/v2"
	log "github.com/micro/go-micro/v2/logger"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	ratelimit "github.com/micro/go-plugins/wrapper/ratelimiter/uber/v2"
	opentracing2 "github.com/micro/go-plugins/wrapper/trace/opentracing/v2"
	"github.com/opentracing/opentracing-go"
)

var QPS = 100

func main() {

	//配置中心
	consulConfig, err := common.GetConsulConfig("127.0.0.1", 8500, "/micro/config")
	if err != nil {
		log.Error(err)
	}
	//注册中心
	//consul := consul2.NewRegistry(func(options *registry.Options) {
	//	options.Addrs = []string{
	//		"127.0.0.1:8500",
	//	}
	//})
	//链路追综
	t, io, err := common.NewTracer("go.micro.service.cart", "127.0.0.1:6831")
	if err != nil {
		log.Error(err)
	}
	defer io.Close()
	opentracing.SetGlobalTracer(t)

	//数据库连接
	//连接数据库
	//获取mysql配置，路径中不带前缀
	mysqlInfo := common.GetMysqlFromConsul(consulConfig, "mysql")
	//连接数据库
	db, err := gorm.Open("mysql", mysqlInfo.User+":"+mysqlInfo.Pwd+"@/"+mysqlInfo.Database+"?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		log.Error(err)
	}
	defer db.Close()
	//禁止复表
	db.SingularTable(true)
	//数据表初始化 不能重复执行
	//rp := repository.NewCartRepository(db)
	//_ = rp.InitTable()

	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.cart"),
		micro.Version("latest"),
		//暴露的服务地址
		micro.Address("127.0.0.1:8087"),
		//注册中心
		//micro.Registry(consul),
		//使用etcd作为注册中心
		micro.Registry(etcd.NewRegistry(
			registry.Addrs("127.0.0.1:2379"))),
		//链路追踪
		micro.WrapHandler(opentracing2.NewHandlerWrapper(opentracing.GlobalTracer())),
		//添加限流
		micro.WrapHandler(ratelimit.NewHandlerWrapper(QPS)),
	)

	// Initialise service
	service.Init()

	// Register Handler
	cartService := service2.NewCartDataService(repository.NewCartRepository(db))
	cart.RegisterCartHandler(service.Server(), handler.Cart{CartDataService: cartService})

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}

package handler

import (
	"context"
	"gitee.com/jhj420/cart/domain/model"
	"gitee.com/jhj420/cart/domain/service"
	"gitee.com/jhj420/cart/proto/cart"
	"gitee.com/jhj420/common"
)

type Cart struct {
	CartDataService service.ICartDataService
}
//添加购物车
func (c Cart) AddCart(ctx context.Context, request *cart.CartInfo, response *cart.ResponseAdd) (err error) {
	cart := &model.Cart{}
	_ = common.SwapTo(request, cart)
	response.CartId,err = c.CartDataService.AddCart(cart)
	return err
}
//清空购物车
func (c Cart) CleanCart(ctx context.Context, request *cart.Clean, response *cart.Response) error {
	if err := c.CartDataService.CleanCart(request.UserId);err != nil {
		return err
	}
	response.Msg = "清空购物车成功"
	return nil
}
//增加商品数量
func (c Cart) Incr(ctx context.Context, request *cart.Item, response *cart.Response) error {
	if err := c.CartDataService.IncrNum(request.Id,request.ChangeNum);err != nil{
		return err
	}
	response.Msg = "添加成功"
	return nil
}
//减少商品数量
func (c Cart) Decr(ctx context.Context, request *cart.Item, response *cart.Response) error {
	if err := c.CartDataService.DecrNum(request.Id,request.ChangeNum);err != nil{
		return err
	}
	response.Msg = "减少成功"
	return nil
}
//删除商品
func (c Cart) DeleteItemByID(ctx context.Context, request *cart.CartID, response *cart.Response) error {
	if err := c.CartDataService.DeleteCart(request.Id);err != nil{
		return nil
	}
	response.Msg = "删除成功"
	return nil
}
//获取购物车内所有商品
func (c Cart) GetAll(ctx context.Context, request *cart.CartFindAll, response *cart.CartAll) error {
	cartAll,err := c.CartDataService.FindAllCart(request.UserId)
	if err != nil{
		return err
	}
	for _,v := range cartAll{
		cart := &cart.CartInfo{}
		if err := common.SwapTo(v,cart);err != nil{
			return err
		}
		response.CartInfo = append(response.CartInfo, cart)
	}
	return nil
}


